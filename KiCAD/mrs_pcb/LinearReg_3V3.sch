EESchema Schematic File Version 4
LIBS:mrs_pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:AMS1117-3.3 U?
U 1 1 5E74AB98
P 6000 3650
AR Path="/5E74AB98" Ref="U?"  Part="1" 
AR Path="/5E745ED1/5E74AB98" Ref="U3"  Part="1" 
F 0 "U3" H 6000 3892 50  0000 C CNN
F 1 "AMS1117-3.3" H 6000 3801 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 6000 3850 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 6100 3400 50  0001 C CNN
	1    6000 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3650 4800 3650
Wire Wire Line
	4600 3950 4800 3950
$Comp
L Device:C C?
U 1 1 5E74ABA1
P 4800 3800
AR Path="/5E74ABA1" Ref="C?"  Part="1" 
AR Path="/5E745ED1/5E74ABA1" Ref="C1"  Part="1" 
F 0 "C1" H 4915 3846 50  0000 L CNN
F 1 "100u" H 4915 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4838 3650 50  0001 C CNN
F 3 "~" H 4800 3800 50  0001 C CNN
	1    4800 3800
	1    0    0    -1  
$EndComp
Connection ~ 4800 3650
Connection ~ 4800 3950
Connection ~ 5400 3650
Wire Wire Line
	5400 3650 5700 3650
Connection ~ 5400 3950
Wire Wire Line
	5400 3950 6000 3950
Wire Wire Line
	4800 3950 5400 3950
Wire Wire Line
	4800 3650 5400 3650
$Comp
L Device:C C?
U 1 1 5E74ABAF
P 5400 3800
AR Path="/5E74ABAF" Ref="C?"  Part="1" 
AR Path="/5E745ED1/5E74ABAF" Ref="C2"  Part="1" 
F 0 "C2" H 5515 3846 50  0000 L CNN
F 1 "100n" H 5515 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5438 3650 50  0001 C CNN
F 3 "~" H 5400 3800 50  0001 C CNN
	1    5400 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E74ABB5
P 6600 3800
AR Path="/5E74ABB5" Ref="C?"  Part="1" 
AR Path="/5E745ED1/5E74ABB5" Ref="C3"  Part="1" 
F 0 "C3" H 6715 3846 50  0000 L CNN
F 1 "100u" H 6715 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6638 3650 50  0001 C CNN
F 3 "~" H 6600 3800 50  0001 C CNN
	1    6600 3800
	1    0    0    -1  
$EndComp
Connection ~ 6600 3650
Connection ~ 6600 3950
Wire Wire Line
	6600 3950 7200 3950
$Comp
L Device:C C?
U 1 1 5E74ABBE
P 7200 3800
AR Path="/5E74ABBE" Ref="C?"  Part="1" 
AR Path="/5E745ED1/5E74ABBE" Ref="C4"  Part="1" 
F 0 "C4" H 7315 3846 50  0000 L CNN
F 1 "100n" H 7315 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7238 3650 50  0001 C CNN
F 3 "~" H 7200 3800 50  0001 C CNN
	1    7200 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3950 6600 3950
Connection ~ 6000 3950
Wire Wire Line
	6600 3650 7200 3650
Wire Wire Line
	6300 3650 6600 3650
$Comp
L power:GND #PWR?
U 1 1 5E74ABC8
P 6000 4050
AR Path="/5E74ABC8" Ref="#PWR?"  Part="1" 
AR Path="/5E745ED1/5E74ABC8" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 6000 3800 50  0001 C CNN
F 1 "GND" H 6005 3877 50  0000 C CNN
F 2 "" H 6000 4050 50  0001 C CNN
F 3 "" H 6000 4050 50  0001 C CNN
	1    6000 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4050 6000 3950
Wire Wire Line
	7200 3650 7200 3550
$Comp
L power:+3.3V #PWR?
U 1 1 5E74ABD0
P 7200 3550
AR Path="/5E74ABD0" Ref="#PWR?"  Part="1" 
AR Path="/5E745ED1/5E74ABD0" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 7200 3400 50  0001 C CNN
F 1 "+3.3V" H 7215 3723 50  0000 C CNN
F 2 "" H 7200 3550 50  0001 C CNN
F 3 "" H 7200 3550 50  0001 C CNN
	1    7200 3550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E74ABD6
P 4800 3550
AR Path="/5E74ABD6" Ref="#PWR?"  Part="1" 
AR Path="/5E745ED1/5E74ABD6" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 4800 3400 50  0001 C CNN
F 1 "+5V" H 4815 3723 50  0000 C CNN
F 2 "" H 4800 3550 50  0001 C CNN
F 3 "" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3550 4800 3650
Text HLabel 4600 3650 0    50   Input ~ 0
Vin
Text HLabel 4600 3950 0    50   Input ~ 0
GND
$EndSCHEMATC
