EESchema Schematic File Version 4
LIBS:mrs_pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U1
U 1 1 5E721244
P 5400 4450
F 0 "U1" H 4900 2950 50  0000 C CNN
F 1 "STM32F103C8Tx" H 5350 2770 50  0001 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 4800 3050 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 5400 4450 50  0001 C CNN
	1    5400 4450
	1    0    0    -1  
$EndComp
$Comp
L RF:NRF24L01_Breakout U2
U 1 1 5E72390D
P 8650 2750
F 0 "U2" H 9030 2796 50  0000 L CNN
F 1 "NRF24L01_Breakout" H 9030 2705 50  0000 L CNN
F 2 "mrs_pcb:NRF24L_SMD" H 8800 3350 50  0001 L CIN
F 3 "http://www.nordicsemi.com/eng/content/download/2730/34105/file/nRF24L01_Product_Specification_v2_0.pdf" H 8650 2650 50  0001 C CNN
	1    8650 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5E722809
P 9400 4750
F 0 "J3" H 9400 4850 50  0000 L CNN
F 1 "Conn_01x02" H 9480 4651 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9400 4750 50  0001 C CNN
F 3 "~" H 9400 4750 50  0001 C CNN
	1    9400 4750
	1    0    0    -1  
$EndComp
$Sheet
S 8200 4550 850  350 
U 5E721430
F0 "NFET_wLED" 50
F1 "NFET_wLED.sch" 50
F2 "Control_Sig" I L 8200 4650 50 
F3 "Switch_Output_High" I R 9050 4750 50 
F4 "Switch_Output_Low" I R 9050 4850 50 
$EndSheet
Wire Wire Line
	9050 4750 9100 4750
Wire Wire Line
	9050 4850 9150 4850
Wire Wire Line
	8200 4650 8050 4650
Text Label 8050 4650 2    50   ~ 0
Switch
Wire Wire Line
	6000 4650 6100 4650
Text Label 6100 4650 0    50   ~ 0
CS
Wire Wire Line
	6000 4750 6100 4750
Text Label 6100 4750 0    50   ~ 0
CLK
Wire Wire Line
	6000 4850 6100 4850
Text Label 6100 4850 0    50   ~ 0
MISO
Wire Wire Line
	6000 4950 6100 4950
Text Label 6100 4950 0    50   ~ 0
MOSI
Wire Wire Line
	8150 2750 8050 2750
Text Label 8050 2750 2    50   ~ 0
CS
Wire Wire Line
	8150 2650 8050 2650
Text Label 8050 2650 2    50   ~ 0
CLK
Wire Wire Line
	8150 2550 8050 2550
Text Label 8050 2550 2    50   ~ 0
MISO
Wire Wire Line
	8150 2450 8050 2450
Text Label 8050 2450 2    50   ~ 0
MOSI
Wire Wire Line
	8150 3050 8050 3050
Text Label 8050 3050 2    50   ~ 0
IRQ
Wire Wire Line
	8150 2950 8050 2950
Text Label 8050 2950 2    50   ~ 0
CE
Wire Wire Line
	6000 5050 6100 5050
Text Label 6100 5050 0    50   ~ 0
IRQ
Wire Wire Line
	6000 5150 6100 5150
Text Label 6100 5150 0    50   ~ 0
CE
Wire Wire Line
	5200 5950 5200 6000
Wire Wire Line
	5200 6000 5300 6000
Wire Wire Line
	5300 6000 5300 5950
Wire Wire Line
	5300 6000 5400 6000
Wire Wire Line
	5400 6000 5400 5950
Connection ~ 5300 6000
Wire Wire Line
	5400 6000 5500 6000
Wire Wire Line
	5500 6000 5500 5950
Connection ~ 5400 6000
Wire Wire Line
	5200 2950 5200 2900
Wire Wire Line
	5200 2900 5300 2900
Wire Wire Line
	5300 2900 5300 2950
Wire Wire Line
	5300 2900 5400 2900
Wire Wire Line
	5400 2900 5400 2950
Connection ~ 5300 2900
Wire Wire Line
	5400 2900 5500 2900
Wire Wire Line
	5500 2900 5500 2950
Connection ~ 5400 2900
Wire Wire Line
	5500 2900 5600 2900
Wire Wire Line
	5600 2900 5600 2950
Connection ~ 5500 2900
Wire Wire Line
	5200 2900 5200 2800
Connection ~ 5200 2900
$Comp
L power:+3.3V #PWR01
U 1 1 5E7276F0
P 5200 2800
F 0 "#PWR01" H 5200 2650 50  0001 C CNN
F 1 "+3.3V" H 5215 2973 50  0000 C CNN
F 2 "" H 5200 2800 50  0001 C CNN
F 3 "" H 5200 2800 50  0001 C CNN
	1    5200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 2150 8650 2050
$Comp
L power:+3.3V #PWR03
U 1 1 5E728433
P 8650 2050
F 0 "#PWR03" H 8650 1900 50  0001 C CNN
F 1 "+3.3V" H 8665 2223 50  0000 C CNN
F 2 "" H 8650 2050 50  0001 C CNN
F 3 "" H 8650 2050 50  0001 C CNN
	1    8650 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5E72910C
P 8650 3450
F 0 "#PWR04" H 8650 3200 50  0001 C CNN
F 1 "GND" H 8655 3277 50  0000 C CNN
F 2 "" H 8650 3450 50  0001 C CNN
F 3 "" H 8650 3450 50  0001 C CNN
	1    8650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3450 8650 3350
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5E72A75B
P 9400 4450
F 0 "J2" H 9400 4550 50  0000 L CNN
F 1 "Conn_01x02" H 9480 4351 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 9400 4450 50  0001 C CNN
F 3 "~" H 9400 4450 50  0001 C CNN
	1    9400 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4550 9150 4550
Wire Wire Line
	9150 4550 9150 4850
Connection ~ 9150 4850
Wire Wire Line
	9150 4850 9200 4850
Wire Wire Line
	9100 4750 9100 4450
Wire Wire Line
	9100 4450 9200 4450
Connection ~ 9100 4750
Wire Wire Line
	9100 4750 9200 4750
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5E74B2A1
P 2000 2150
F 0 "J1" H 2000 2250 50  0000 L CNN
F 1 "Conn_01x02" H 2080 2051 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2000 2150 50  0001 C CNN
F 3 "~" H 2000 2150 50  0001 C CNN
	1    2000 2150
	-1   0    0    1   
$EndComp
$Sheet
S 2300 1950 550  300 
U 5E745ED1
F0 "LinearReg_3V3" 50
F1 "LinearReg_3V3.sch" 50
F2 "Vin" I L 2300 2050 50 
F3 "GND" I L 2300 2150 50 
$EndSheet
Wire Wire Line
	2200 2050 2300 2050
Wire Wire Line
	2200 2150 2300 2150
Connection ~ 5200 6000
Wire Wire Line
	5200 6100 5200 6000
$Comp
L power:GND #PWR02
U 1 1 5E724E80
P 5200 6100
F 0 "#PWR02" H 5200 5850 50  0001 C CNN
F 1 "GND" H 5205 5927 50  0000 C CNN
F 2 "" H 5200 6100 50  0001 C CNN
F 3 "" H 5200 6100 50  0001 C CNN
	1    5200 6100
	1    0    0    -1  
$EndComp
$Sheet
S 3250 3900 550  250 
U 5E758E48
F0 "8M_crystal" 50
F1 "8M_crystal.sch" 50
F2 "Osc_In" I R 3800 3950 50 
F3 "Osc_Out" I R 3800 4050 50 
$EndSheet
Wire Wire Line
	3800 4050 4700 4050
Wire Wire Line
	3800 3950 4700 3950
$EndSCHEMATC
