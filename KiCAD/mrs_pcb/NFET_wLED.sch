EESchema Schematic File Version 4
LIBS:mrs_pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5E72A863
P 5950 3600
AR Path="/5E72A863" Ref="Q?"  Part="1" 
AR Path="/5E726B6C/5E72A863" Ref="Q1"  Part="1" 
AR Path="/5E721430/5E72A863" Ref="Q1"  Part="1" 
F 0 "Q1" H 6156 3646 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 6156 3555 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6150 3700 50  0001 C CNN
F 3 "~" H 5950 3600 50  0001 C CNN
	1    5950 3600
	1    0    0    -1  
$EndComp
Text Label 5650 3600 2    50   ~ 0
Gate
Text Label 6050 3885 0    50   ~ 0
Source
$Comp
L Device:R R2
U 1 1 5E72A881
P 5305 3600
F 0 "R2" V 5098 3600 50  0000 C CNN
F 1 "1k" V 5189 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5235 3600 50  0001 C CNN
F 3 "~" H 5305 3600 50  0001 C CNN
	1    5305 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	5455 3600 5750 3600
Wire Wire Line
	5155 3600 5035 3600
$Comp
L Device:LED D1
U 1 1 5E72A88A
P 5645 4390
F 0 "D1" H 5638 4135 50  0000 C CNN
F 1 "LED" H 5638 4226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 5645 4390 50  0001 C CNN
F 3 "~" H 5645 4390 50  0001 C CNN
	1    5645 4390
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5E72A890
P 5300 4390
F 0 "R1" V 5093 4390 50  0000 C CNN
F 1 "1k" V 5184 4390 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5230 4390 50  0001 C CNN
F 3 "~" H 5300 4390 50  0001 C CNN
	1    5300 4390
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 4390 5495 4390
$Comp
L power:GND #PWR05
U 1 1 5E72A897
P 5890 4455
F 0 "#PWR05" H 5890 4205 50  0001 C CNN
F 1 "GND" H 5895 4282 50  0000 C CNN
F 2 "" H 5890 4455 50  0001 C CNN
F 3 "" H 5890 4455 50  0001 C CNN
	1    5890 4455
	1    0    0    -1  
$EndComp
Wire Wire Line
	5795 4390 5890 4390
Wire Wire Line
	5890 4390 5890 4455
Wire Wire Line
	5035 4390 5150 4390
Connection ~ 5035 3600
Wire Wire Line
	5035 3600 5035 4390
Text HLabel 4400 3600 0    50   Input ~ 0
Control_Sig
Wire Wire Line
	4400 3600 5035 3600
Text HLabel 6150 3150 2    50   Input ~ 0
Switch_Output_High
Wire Wire Line
	6050 3400 6050 3150
Text Label 6050 3300 0    50   ~ 0
Drain
Wire Wire Line
	6150 3150 6050 3150
Wire Wire Line
	6050 3950 6150 3950
Wire Wire Line
	6050 3800 6050 3950
Text HLabel 6150 3950 2    50   Input ~ 0
Switch_Output_Low
$EndSCHEMATC
