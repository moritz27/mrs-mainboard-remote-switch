EESchema Schematic File Version 4
LIBS:mrs_pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Crystal Y1
U 1 1 5E759B40
P 5750 3700
F 0 "Y1" V 5796 3569 50  0000 R CNN
F 1 "Crystal" V 5705 3569 50  0000 R CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 5750 3700 50  0001 C CNN
F 3 "~" H 5750 3700 50  0001 C CNN
	1    5750 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 5E759B46
P 5400 3450
F 0 "C5" V 5550 3500 50  0000 L CNN
F 1 "22p" V 5550 3350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5438 3300 50  0001 C CNN
F 3 "~" H 5400 3450 50  0001 C CNN
	1    5400 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C6
U 1 1 5E759B4C
P 5400 3950
F 0 "C6" V 5550 4050 50  0000 C CNN
F 1 "22p" V 5550 3850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5438 3800 50  0001 C CNN
F 3 "~" H 5400 3950 50  0001 C CNN
	1    5400 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 3450 5750 3450
Wire Wire Line
	5750 3450 5750 3550
Wire Wire Line
	5550 3950 5750 3950
Wire Wire Line
	5750 3950 5750 3850
Wire Wire Line
	5250 3950 5100 3950
Wire Wire Line
	5100 3950 5100 3450
Wire Wire Line
	5100 3450 5250 3450
$Comp
L power:GND #PWR0101
U 1 1 5E759B59
P 5100 4050
F 0 "#PWR0101" H 5100 3800 50  0001 C CNN
F 1 "GND" H 5105 3877 50  0000 C CNN
F 2 "" H 5100 4050 50  0001 C CNN
F 3 "" H 5100 4050 50  0001 C CNN
	1    5100 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4050 5100 3950
Text HLabel 6250 3450 2    50   Input ~ 0
Osc_In
Text HLabel 6250 3950 2    50   Input ~ 0
Osc_Out
Wire Wire Line
	5750 3450 6250 3450
Connection ~ 5750 3450
Wire Wire Line
	5750 3950 6250 3950
Connection ~ 5750 3950
$EndSCHEMATC
